#!/bin/bash
basepath=$(pwd)

# to avod issues with btbucket the . as hidded files is removed
	mkdir ../dotfiles
echo  "copying .dot files settings"
	cd $basepath
#echo "copy .profile - set the default applications like terminal, webbrowser"
#	cp ~/.profile ../dotfiles/profile
echo "copy .bin scripts"
	cp ~/.bin ../dotfiles/bin -r
	cp ~/.bashrc ../dotfiles/bashrc

echo "copy feh settings, now to put single wallpaper on each monitor"
	cp ~/.fehbg ../dotfiles/fehbg
echo "copy URXVT settings - .Xresources"
	cp ~/.Xresources ../dotfiles/Xresources
echo "setup multi screens"
	cp ~/.screenlayout ../dotfiles/screenlayout -r
echo "copying .config directory"
	mkdir ../dotfiles/config
	cp ~/.config/bspwm ../dotfiles/config/ -r
	cp ~/.config/sxhkd ../dotfiles/config/ -r
	