# bspwm

#### After fresh installation

- generic apps
`sudo pacman -Sy keepassxc nexcloud-client falkon telegram-desktop`

- AUR repositories
    * pamac installer
`sudo pacman install lxsession pamac-cli`
    * markdown editor
`pamac remarkable`
    * font for terminal rxvt
`pamac build ttf-iosevka`

- terminal rvxt
`sudo pacman -S rxvt-unicode xsel urxvt-perls`

    _reference from this_ [site](https://addy-dclxvi.github.io/post/configuring-urxvt/) and settings are file `~/.Xresource`

- terminal hyper
`sudo pacman -S hyper`

    project [site](https://hyper.is/)  and settings are  `~/.hyper.js`
   
    _do not recomend in May 2019: quite slow to load, you can get nice view on rxvt_

- terminal sterminal
it's the default manjaro terminal and settings are in ` ~.sterminalrc`

#### polybar

To install polybar and use the default settings
```
sudo pacman -S polybar -y
cd
cd .config
mkdir polybar
cp /usr/share/doc/polybar/config polybar/
cd polybar
sudo killall limepanel
polybar &
```

### where are the settings
* Defaut applications like the terminal and browser
`micro ~/.profile`
* Wallpaper when using feh
`~/.fehbg`
* URXVT
 `~/.Xresource`
 
 
#### Others links and references
* nice site to collect colors: <https://terminal.sexy/>


to check 
https://github.com/saimn/dotfiles/blob/master/Xresources


### Funny things

toilet -f <font> "text" | lolcat
fonts are located in /usr/share/figlet

### Tools
check the font
for FontAwesome ctrl+f and search the unicode
'sudo pacman -S guacharmap'
